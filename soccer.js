(function () {

const RESULT_VALUES = { w: 3, d: 1, l: 0 }

/**
 * Takes a single result string and (one of 'w', 'l', or 'd') 
 * and returns the point value
 * 
 * @param {string} result 
 * @returns {number} point value
 */
const getPointsFromResult = function getPointsFromResult(result) {
  return RESULT_VALUES[result];
}

// Create getTotalPoints function which accepts a string of results
// including wins, draws, and losses i.e. 'wwdlw'
// Returns total number of points won

const reducer = (accumulator, currentValue) => accumulator + currentValue;

function getTotalPoints(results) {
  let arr = results.split('');
  let totalArr = [];
  arr.forEach(function(score) {
    let result = totalArr.push(getPointsFromResult(score));
  })
  return totalArr.reduce(reducer);
}

// Check getTotalPoints
console.log(getTotalPoints('wwdl')); // should equal 7

// create orderTeams function that accepts as many team objects as desired, 
// each argument is a team object in the format { name, results }
// i.e. {name: 'Sounders', results: 'wwlwdd'}
// Logs each entry to the console as "Team name: points"


function orderTeams(...arguments) {
  let arr = Array.from(arguments);
  arr.forEach(function(item) {
    console.log(item.teamName + ': ' + getTotalPoints(item.results));
  });
  return;
}

// Check orderTeams
orderTeams(
  { teamName: 'Sounders', results: 'wwdl' },
  { teamName: 'Galaxy', results: 'wlld' }
);
// should log the following to the console:
// Sounders: 7
// Galaxy: 4

})();